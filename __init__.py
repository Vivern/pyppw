import subprocess
import getpass
import os
import json
import sys
import readline
import mpw.algorithm.v3 as pympw
from pathlib import Path


HERE       = Path(__file__).parent
CONFIG_DIR = Path.home() / '.config' / 'pyppw'
DBPATH     = CONFIG_DIR / 'db.json'


class UserError(Exception):
    pass


class DBEntry:
    def __init__(self, name, site, misc):
        assert isinstance(name, str), name
        assert isinstance(site, str), site
        assert isinstance(misc, dict), misc

        for k, v in misc.items():
            assert isinstance(k, str), k
            assert isinstance(v, str), v

        assert 'username' not in misc
        assert 'site' not in misc

        self.name = name
        self.site = site
        self.misc = misc

    @staticmethod
    def from_json(ser):
        if not isinstance(ser, dict):
            raise ValueError('the value is not a dict')

        misc = ser.copy()
        try:
            name = misc.pop('username')
            site = misc.pop('site')
        except KeyError as err:
            raise ValueError('the value is missing the key "{}"'.format(err))

        if not isinstance(name, str):
            raise ValueError('the name is not a string: "{}"'.format(name))

        if not isinstance(site, str):
            raise ValueError('the site is not a string: "{}"'.format(site))

        return DBEntry(name, site, misc)

    def as_json(self):
        result = self.misc.copy()
        result['username'] = self.name
        result['site']     = self.site

        return result


def runMPW(name, site, password):
    mkey = pympw.get_master_key(password, name)
    return pympw.get_password(mkey, site, 1, pympw.PwdType.long, pympw.Scope.password, '')


def get_entry(site):
    assert isinstance(site, str), site
    for char in site:
        assert not char.isspace(), repr(char)

    cands = {}
    for entry in db:
        if site in entry.site:
            try:
                cands[site].append(entry)
            except KeyError:
                cands[site] = [entry]

    if len(cands) == 0:
        raise UserError('no entry matches the site "{}"'.format(site))

    if site in cands:
        return cands[site]

    if len(cands) == 1:
        return cands.popitem()

    msg = []
    msg.append('The site "{}" is not unique. Candidates are:'.format(site))
    for cand in cands.keys():
        msg.append('    • {}'.format(cand))
    raise UserError('\n'.join(msg))


def handleCommand(command):
    command = command.strip()
    command = command.split()
    if len(command) == 0:
        return

    elif command[0] == 'help':
        if len(command) != 1:
            raise UserError('the "help" command takes no arguments')
        print('commands are: help, add, get, compute, list')

    elif command[0] == 'add':
        if len(command) != 3:
            raise UserError('the "add" command requires exactly two arguments: <name> <site>')

        name, site = command[1:]
        entry = DBEntry(name, site, {})
        db.append(entry)
        print('success: {}'.format(runMPW(name, site, password)))

    elif command[0] == 'get':
        if len(command) != 2:
            raise UserError('the "get" command requires exactly one argument: <site>')

        entries = get_entry(command[1])
        for entry in entries:
            print('site: {}'.format(entry.site))
            print('    {:>30}  {}'.format(entry.name,
                                          runMPW(entry.name, entry.site, password)))

    elif command[0] == 'compute':
        if len(command) != 3:
            raise UserError('the "compute" command requires exactly two arguments: <username> <site>')

        _, name, site = command

        print(runMPW(name, site, password))

    elif command[0] == 'list':
        if len(command) != 1:
            raise UserError('the "list" command takes no arguments')

        for entry in db:
            print('    {:>30} @ {}'.format(entry.name, entry.site))

    else:
        print('unrecognized command')
        return


def opendb():
    global db

    try:
        rawdb = json.load(DBPATH.open())
        db = []
        for entry in rawdb:
            db.append(DBEntry.from_json(entry))

    except FileNotFoundError:
        print('Database not found. CREATING A NEW, EMPTY ONE.')
        db = []

    except Exception as err:
        print('Failed to load the database: {}'.format(err))
        sys.exit()


def storedb():
    jsondb = []
    for entry in db:
        jsondb.append(entry.as_json())

    os.makedirs(CONFIG_DIR, exist_ok=True)
    json.dump(jsondb, DBPATH.open('w'))


def run():
    global password

    try:
        password = getpass.getpass('master password: ')
    except EOFError:
        print()
        return

    check    = runMPW('a', 'b', password)
    print(check)
    print()

    opendb()

    # initialize readline
    histfile = os.path.join(os.path.expanduser('~'), '.pyppw_history')
    try:
        readline.read_history_file(histfile)
        readline.set_history_length(1000)
    except FileNotFoundError:
        pass

    print('Type "help" to see all commands')

    while True:
        try:
            cmd = input('> ')
        except EOFError:
            print()
            break

        try:
            handleCommand(cmd)
        except UserError as err:
            print('Error: {}'.format(err))

    storedb()
    readline.write_history_file(histfile)


if __name__ == '__main__':
    run()

